package drinks.client;

import drinks.entity.Drink;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
public class DrinksClient {
    public static final String  HOST_URL = "http://127.0.0.1:8080";

    public static void main(String[] args) throws InterruptedException {
        WebClient webClient = WebClient.create(HOST_URL);

        Mono<Drink> drink = webClient
                .get()
                .uri("/drinks/{id}", 1)
                .retrieve()
                .bodyToMono(Drink.class)
                .doOnError(e -> log.error("Error on request: ", e));

        
        drink.subscribe(d -> log.info("Received drink: " + d.getName()));

        Flux<Drink> drinks = webClient
                .get()
                .uri("/drinks")
                .retrieve()
                .bodyToFlux(Drink.class);

        drinks.subscribe(d -> log.info("Available drinks: " + d.getName()));

        //TODO: If going to use this for real, this is definitely not way to do it :)
        Thread.sleep(2000);

    }
}
