package drinks.app.repo;

import drinks.entity.Drink;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DrinksRepository {

    private static final Map<Integer, Drink> DRINKS = new HashMap<>();

    static {

        List.of(
                new Drink(0, "Water"),
                new Drink(1, "Juice"),
                new Drink(2, "Tea"),
                new Drink(3, "Coffe")).forEach(d -> DRINKS.put(d.getId(), d));

    }

    public Flux<Drink> findAll() {
        return Flux.fromIterable(DRINKS.values());
    }

    public Mono<Drink> findById(Integer id) {
        return Mono.just(DRINKS.get(id));
    }

}
