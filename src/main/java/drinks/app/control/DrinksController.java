package drinks.app.control;

import drinks.app.repo.DrinksRepository;
import drinks.entity.Drink;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/drinks")
@RequiredArgsConstructor
public class DrinksController {
    private final DrinksRepository drinksRepository;

    @GetMapping
    private Flux<Drink> getDrinks() {
        return drinksRepository.findAll();
    }

    @GetMapping("/{id}")
    private Mono<Drink> getDrinkById(@PathVariable String id) {
        try {
            return drinksRepository.findById(Integer.parseInt(id));
        } catch (NumberFormatException nfe) {
            return Mono.error(nfe);
        }

    }

}
